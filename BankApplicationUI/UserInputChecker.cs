﻿using System;
namespace BankApplicationUI
{
	public static class UserInputChecker
	{
		public static int GetUserChoise(string message, string errorMessage = "Incorrect input!")
		{
			int count;
			string input;

			Console.Write(message);

			input = Console.ReadLine();

			while (!Int32.TryParse(input, out count))
			{
				Console.WriteLine(errorMessage);
				Console.Write(message);
				input = Console.ReadLine();
			}

			return count;
		}

		public static double GetUserCount(string message, string errorMessage = "Incorrect input!")
		{
			double count;
			string input;

			Console.Write(message);

			input = Console.ReadLine();

			while (!Double.TryParse(input, out count))
			{
				Console.WriteLine(errorMessage);
				Console.Write(message);
				input = Console.ReadLine();
			}

			return count;
		}

		public static string GetUserInput(string message, string errorMessage = "Incorrect input!")
		{
			bool success;
			string input;

			do
			{
				Console.Write(message);

				input = Console.ReadLine();
				success = !string.IsNullOrWhiteSpace(input);

				if (!success)
				{
					Console.WriteLine(errorMessage);
				}
			} while (!success);

			return input;
		}
	}
}
