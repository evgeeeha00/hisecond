﻿using System;
using System.Collections.Generic;
using BankApplicationBLL;
using BankApplicationUI.Menu;
using static BankApplicationUI.Menu.NotVoidMenu<string>;

namespace BankApplicationUI
{
	class MenuItems
	{
		const int DifferenceBetweenMenuItemAndPlaceInArray = 1;

		// Main menu functions
		internal static void LoginAndUserMenu()
		{
			string userLogin;
			if (UserLogin(out userLogin))
			{
				UserMenu(userLogin);
			}
		}

		internal static void RegistrationAndUserMenu()
		{
			string userLogin;
			if (UserRegistration(out userLogin))
			{
				UserMenu(userLogin);
			}
		}

		// Main menu subfunctions
		private static void UserMenu(string userLogin)
		{
			NotVoidMenu<string> userMenu = new NotVoidMenu<string>(new string[] 
				{ "Get credit card", "Get debit card", "Show cards", "Transfer money from one card to another", "Put money on the card", "Withdraw money from the card" },
				new Function[] { GetCreditCard, GetDebitCard, ShowCards, TransferMoney, PutMoney, WithdrawMoney });

			userMenu.RunMenu(userLogin, "Exit in main menu..."); 
		}

		internal static bool UserRegistration(out string userLogin)
		{
			string name, surname, password, repeatPassword, login;

			login = UserInputChecker.GetUserInput("Input Login: ");
			name = UserInputChecker.GetUserInput("Input Name: ");
			surname = UserInputChecker.GetUserInput("Input Surame: ");

			userLogin = login;

			while (true)
			{
				password = UserInputChecker.GetUserInput("Input password: ");
				repeatPassword = UserInputChecker.GetUserInput("Repeat password: ");

				Console.Clear();
				if (password == repeatPassword) break;
				else Console.WriteLine("Passwords don't match, please try again.");
			}

			if (Bank.AddUser(login, name, surname, password))
			{
				Console.WriteLine("Registration failed!");
				return false;
			}
			return true;
		}

		public static bool UserLogin(out string userLogin)
		{
			string login, password;

			login = UserInputChecker.GetUserInput("Input Login: ", "Incorrect input!");
			password = UserInputChecker.GetUserInput("Input password: ", "Incorrect input!");
			
			userLogin = login;

			switch (Bank.FindUserCard(login, password))
			{
				case 0:
					Console.WriteLine("Login completed successfully!");
					return true;
				case 1:
				case 2:
					Console.WriteLine("Incorrect username or password!");
					return false;
				default:
					return false;
			}
		}

		// User menu functions
		public static void GetCreditCard(string userLogin)
		{
			double count = UserInputChecker.GetUserCount("Enter the loan amount: ", "Incorrect input!");
			if (Bank.AddCreditCard(userLogin, count))
			{
				Console.WriteLine("Credit card has been successfully created!");
			}
		}

		public static void GetDebitCard(string userLogin)
		{
			if (Bank.AddDebitCard(userLogin))
			{
				Console.WriteLine("Debit card has been successfully created!");
			}
		}

		public static void ShowCards(string userLogin)
		{
			List<CardInfo> types = Bank.GetUserCards(userLogin);
			if(types.Count == 0 || types == null)
			{
				Console.WriteLine("The user does not have any cards!");
			}
			else
			{
				int k = 1;
				foreach (CardInfo card in types)
				{
					Console.WriteLine($"{k}. {card.Type.ToString().Substring(0,1)} {card.Id.Substring(0, 4)} {card.Id.Substring(4, 4)} {card.Id.Substring(8, 4)} {card.Id.Substring(12, 4)} || {card.Name} {card.Surname} || {card.Money}$"); ///////////////////////////////ТУТ ТОЖЕ КАКАЯ-ТО ДИЧЬ
					k++;
				}
			}
			Console.WriteLine(new string('=', 64));
		}

		public static void PutMoney(string userLogin)
		{
			ShowCards(userLogin);

			List<CardInfo> types = Bank.GetUserCards(userLogin);
			if (types.Count == 0)
			{
				return;
			}

			int choise = UserInputChecker.GetUserChoise("Select the card you want to deposit money on: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			while (choise > types.Count)
			{
				Console.WriteLine("There is no such card number.");
				choise = UserInputChecker.GetUserChoise("Select the card you want to deposit money on: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			}

			double amount = UserInputChecker.GetUserCount("Enter how much money you want to deposit: ");

			try
			{
				Bank.PutMoneyForUser(userLogin, types[choise].Id, amount);
			}
			catch (Exception e)
			{
				Console.WriteLine($"{e.Message} The operation is aborted!");
				return;
			}

			Console.WriteLine("The amount has been successfully credited!");
		}

		public static void WithdrawMoney(string userLogin)
		{
			ShowCards(userLogin);

			List<CardInfo> types = Bank.GetUserCards(userLogin);
			if (types.Count == 0)
			{
				return;
			}

			int choise = UserInputChecker.GetUserChoise("Select the number of the card you want to withdraw money from: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			while (choise > types.Count)
			{
				Console.WriteLine("There is no such card number.");
				choise = UserInputChecker.GetUserChoise("Select the number of the card you want to withdraw money from: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			}

			double amount = UserInputChecker.GetUserCount("Enter how much money you want to deposit: ");

			try
			{
				Bank.WithdrawMoneyForUser(userLogin, types[choise].Id, amount);
			}
			catch (Exception e)
			{
				Console.WriteLine($"{e.Message} The operation is aborted!");
				return;
			}

			Console.WriteLine("The amount was successfully withdrawn!");
		}

		public static void TransferMoney(string userLogin)
		{
			ShowCards(userLogin);

			List<CardInfo> types = Bank.GetUserCards(userLogin);
			if (types.Count == 0)
			{
				return;
			}

			int choiseFirst = UserInputChecker.GetUserChoise("Select the card to transfer money from: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			while (choiseFirst > types.Count)
			{
				Console.WriteLine("There is no such card number.");
				choiseFirst = UserInputChecker.GetUserChoise("Select the card to transfer money from: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			}

			int choiseSecond = UserInputChecker.GetUserChoise("Select the card to transfer money to: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			while (choiseSecond > types.Count)
			{
				Console.WriteLine("There is no such card number.");
				choiseSecond = UserInputChecker.GetUserChoise("Select the card to transfer money to: ") - DifferenceBetweenMenuItemAndPlaceInArray;
			}

			double amount = UserInputChecker.GetUserCount("Enter how much money you want to deposit: ");

			try
			{
				Bank.TranserMoneyForUser(userLogin, types[choiseFirst].Id, types[choiseSecond].Id, amount);
			}
			catch (Exception exception)
			{
				Console.WriteLine($"{exception.Message} The operation is aborted!");
				return;
			}

			Console.WriteLine("The transfer was successfully completed!");
		}
	}
}
