﻿using System;
namespace BankApplicationUI.Menu
{
	public class VoidMenu
	{
		public delegate void Function();

		private string[] Points;
		private Function[] Functions;

		public VoidMenu() { }

		public VoidMenu(string[] points, Function[] functions)
		{
			if (points.Length != functions.Length)
			{
				throw new ArgumentException("The number of menu items and functions must match.");
			}

			Points = points;
			Functions = functions;
		}

		public void RunMenu(string exitItem = "Exit...")
		{
			const int DifferenceBetweenMenuItemAndPlaceInArray = 1;

			while (true)
			{
				for (int i = 0; i < Points.Length; i++)
				{
					Console.WriteLine($"{i + 1}. {Points[i]}");
				}
				Console.WriteLine($"{Points.Length + 1}. {exitItem}");

				int input;
				input = UserInputChecker.GetUserChoise(">> ", "Incorrect input!") - DifferenceBetweenMenuItemAndPlaceInArray;

				Console.Clear();

				if (input == Points.Length)
				{
					return;
				}
				else if (input > Points.Length || input < 0)
				{
					Console.Write("There is no such menu item! Please try again\n");
				}
				else
				{
					Functions[input]();
				}
			}
		}
	}
}
