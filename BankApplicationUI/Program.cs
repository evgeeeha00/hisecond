﻿using BankApplicationUI.Menu;
using static BankApplicationUI.Menu.VoidMenu;
using static BankApplicationUI.MenuItems;

namespace BankApplicationUI
{
	class Program
	{
		static void Main(string[] args)
		{
			VoidMenu menu= new VoidMenu(new string[] { "Log in...", "Registration..." }, new Function[]{ LoginAndUserMenu, RegistrationAndUserMenu });
			menu.RunMenu();
		}
	}
}
