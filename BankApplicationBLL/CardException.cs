﻿using System;
namespace BankApplicationBLL.CardExceptions
{
	public class CardException : Exception
	{
		public CardException(string message)
				: base(message)
		{ }
	}

	public class DebitCardException : CardException
	{
		public DebitCardException(string message)
				: base(message)
		{ }
	}

	public class DebitCardWithdrawException : DebitCardException
	{
		public DebitCardWithdrawException(string message)
				: base(message)
		{ }
	}

	public class CreditCardException : CardException
	{
		public CreditCardException(string message)
				: base(message)
		{ }
	}

	public class CreditCardWithdrawException : CreditCardException
	{
		public CreditCardWithdrawException(string message)
				: base(message)
		{ }
	}

	public class CreditCardPutException : CreditCardException
	{
		public CreditCardPutException(string message)
				: base(message)
		{ }
	}
}
