﻿using System;
using System.Collections.Generic;
namespace BankApplicationBLL
{
	public static class Bank
	{
		private static List<User> Users = new List<User>(); // DAL

		private static string GenerateCardId()
		{
			string cardId = "";
			
			byte[] data = Guid.NewGuid().ToByteArray();
			foreach(byte b in data){
				cardId += Convert.ToInt32(b) % 10;
			}
			return cardId;
		}

		private static int GenerateCardCVV()
		{
			Random random = new Random();
			return random.Next(100, 1000);
		}

		public static bool AddUser(string login, string name, string surname, string password)
		{
			try
			{
				User user = new User(login, name, surname, password);
				Users.Add(user);
			}
			catch {
				return true;
			}
			return false;
		}

		public static bool AddCreditCard(string userLogin, double amount)
		{
			foreach (User user in Users)
			{
				if (user.Login == userLogin)
				{
					user.CreateCreditCard(GenerateCardId(), GenerateCardCVV(), amount);
					return true;
				}
			}
			return false;
		}

		public static bool AddDebitCard(string userLogin)
		{
			foreach (User user in Users)
			{
				if (user.Login == userLogin)
				{
					user.CreateDebitCard(GenerateCardId(), GenerateCardCVV());
					return true;
				}
			}
			return false;
		}

		public static List<CardInfo>? GetUserCards(string userLogin)
		{
			foreach (User user in Users)
			{
				if (user.Login == userLogin)
				{
					return user.GetAllCards();
				}
			}
			return null;
		}

		/// <returns>
		///	0 - The user is found, the password is correct
		///	1 - The user is not found
		///	2 - The user is found, the password is not correct
		/// </returns>
		public static int FindUserCard(string login, string password)
		{
			foreach (User user in Users)
			{
				if (user.Login == login)
				{
					if (user.CheckPassword(password))
					{
						return 0;
					}
					else
					{
						return 2;
					}
				}
			}
			return 1;
		}

		public static void PutMoneyForUser(string userLogin, string cardId, double amount)
		{
			foreach (User user in Users)
			{
				if (user.Login == userLogin)
				{
					user.PutMoneyOnCard(cardId, amount);
					return;
				}
			}
		}

		public static void WithdrawMoneyForUser(string userLogin, string cardId, double amount)
		{
			foreach (User user in Users)
			{
				if (user.Login == userLogin)
				{
					user.WithdrawMoneyFromCard(cardId, amount);
					return;
				}
			}
		}

		public static void TranserMoneyForUser(string userLogin, string firstCardId, string secondCardId, double amount)
		{
			foreach (User user in Users)
			{
				if (user.Login == userLogin)
				{
					user.TransferMoneyFromOneCardFromAnother(firstCardId, secondCardId, amount);
					return;
				}
			}
		}
	}
}
