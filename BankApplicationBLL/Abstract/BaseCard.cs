using System;
using BankApplicationBLL.CardExceptions;

namespace BankApplicationBLL.Abstract
{
	public enum CardType
	{
		Credit,
		Debit
	}

	internal abstract class BaseCard
	{
		public CardType Type { get; protected set; }
		public string CardId { get; private set; }
		public int CVV { get; private set; }
		public string HolderName { get; private set; }
		public string HolderSurname { get; private set; }
		public double Balance { get; protected set; }

		protected BaseCard(string name, string surname, string id, int cvv)
		{
			HolderName = name;
			HolderSurname = surname;
			CardId = id;
			CVV = cvv;
		}

		public abstract void WithdrawMoney(double count);
		public abstract void PutMoney(double count);

		public static void TransferMoney(BaseCard firstCard, BaseCard secondCard, double count)
		{
			try
			{
				firstCard.WithdrawMoney(count);
				secondCard.PutMoney(count);
			}
			catch(CreditCardPutException)
			{
				firstCard.PutMoney(count);
				throw;
			}
			catch(Exception)
			{
				throw;
			}
		}
	}
}