﻿using System;
using System.Collections.Generic;
using BankApplicationBLL.Abstract;
namespace BankApplicationBLL
{
	public struct CardInfo
	{
		public CardType Type { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public double Money { get; set; }
		public string Id { get; set; }

		internal CardInfo(CardType type, string name, string surname, double money, string id)
		{
			Type = type;
			Name = name;
			Surname = surname;
			Money = money;
			Id = id;
		}
	}

	internal class User
	{
		private List<BaseCard> cards = new(); // DAL
		private int passwordHash;							// DAL

		public string Login { get; private set; }
		public string Name { get; private set; }
		public string Surname { get; private set; }

		public User(string login, string name, string surname, string password) 
			{ Login = login; Name = name;  Surname = surname; passwordHash = password.GetHashCode(); }

		public bool CheckPassword(string password)
		{
			return passwordHash == password.GetHashCode();
		}

		public void CreateCreditCard(string cardId, int cvv, double amount)
		{
			cards.Add(new CreditCard(Name, Surname, cardId, cvv, amount));
		}

		public void CreateDebitCard(string cardId, int cvv)
		{
			cards.Add(new DebitCard(Name, Surname, cardId, cvv));
		}

		public List<CardInfo> GetAllCards()
		{
			List<CardInfo> cardList = new();
			foreach(BaseCard c in cards)
			{
				cardList.Add(new CardInfo(c.Type, c.HolderName, c.HolderSurname, c.Balance, c.CardId));
			}
			return cardList;
		}

		public void PutMoneyOnCard(string cardId, double amount)
		{
			foreach(BaseCard card in cards)
			{
				if(card.CardId == cardId)
				{
					card.PutMoney(amount);
					return;
				}
			}
		}

		public void WithdrawMoneyFromCard(string cardId, double amount)
		{
			foreach (BaseCard card in cards)
			{
				if (card.CardId == cardId)
				{
					card.WithdrawMoney(amount);
					return;
				}
			}
		}

		public void TransferMoneyFromOneCardFromAnother(string firstCardId, string secondCardId, double amount)
		{
			BaseCard? first = null;
			BaseCard? second = null;

			// If stopCycle == 2 then 2 cards for transfer were found.
			int stopCycle = 0;

			foreach (BaseCard card in cards)
			{
				if (firstCardId == card.CardId && first == null)
				{
					first = card;
					stopCycle++;
				}
				if (secondCardId == card.CardId && second == null)
				{
					second = card;
					stopCycle++;
				}
				if (stopCycle == 2)
				{
					break;
				}
			}

			if (first == null || second == null)
			{
				throw new NullReferenceException("Cards not exist.");
			}
			else
			{
				BaseCard.TransferMoney(first, second, amount);
			}
		}
	}
}
