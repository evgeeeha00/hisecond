using BankApplicationBLL.Abstract;
using BankApplicationBLL.CardExceptions;

namespace BankApplicationBLL
{
	internal class DebitCard : BaseCard
	{
		public DebitCard(string name, string surname, string id, int cvv)
			: base(name, surname, id, cvv) { Type = CardType.Debit; }

		public override void WithdrawMoney(double count)
		{
			if (Balance >= count)
			{
				Balance -= count;
			}
			else
			{
				throw new DebitCardWithdrawException("The amount on the card is less than the one that is planned to be withdrawn.");
			}
		}

		public override void PutMoney(double count)
		{
			Balance += count;
		}
	}
}