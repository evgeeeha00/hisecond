using BankApplicationBLL.Abstract;
using BankApplicationBLL.CardExceptions;

namespace BankApplicationBLL
{
	internal class CreditCard : BaseCard
	{
		public CreditCard(string name, string surname, string id, int cvv, double credit = 0)
			: base(name, surname, id, cvv) { Balance = credit; Type = CardType.Credit; }

		public override void WithdrawMoney(double Count) 
		{ 
			throw new CreditCardWithdrawException("Money cannot be debited from a credit card."); 
		}

		public override void PutMoney(double count)
		{
			if (Balance >= count)
			{
				Balance -= count;
			}
			else
			{
				throw new CreditCardPutException("The amount on the card is less than the one that you plan to rely on.");
			}
		}
	}
}